"""Embed images as single frames in video"""
'''
    stools
    Copyright (C) 2018 Samuel First

    This file is part of stools.

    stools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    stools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with stools.  If not, see <https://www.gnu.org/licenses/>.
'''

import argparse
from moviepy.video.VideoClip import ImageClip
from moviepy.video.io.editor import VideoFileClip
from moviepy.editor import concatenate_videoclips

# Define Arguments
#  Positional:
#   images: paths to images to hide in video
#   video: path to video to hide/look for images in
#  Flags:
#   -e: embed images in video
#   -d: look for images in video
#   -o: path to save embedded video to
#   -s: number of seconds between images
#   -n: number of images to look for in video
parser = argparse.ArgumentParser(
    description ='Hide images in video as single frames')

parser.add_argument('images',
                    type=str,
                    nargs='?',
                    description='Paths to images to embed in video')

parser.add_argument('video',
                    type=str,
                    nargs='?',
                    description='Name of video to use when embedding\
                    or deembedding')

parser.add_argument('-o',
                    '--output',
                    type=str,
                    description='Name of video to save to')

parser.add_argument('-e',
                    '--embed',
                    action='store_true',
                    description='Embed images in video')

parser.add_argument('-s',
                    '--seconds',
                    type=int,
                    description='Number of seconds between each image')

parser.add_argument('-d',
                    '--deembed',
                    action='store_true',
                    description='Retrieve images from video')

parser.add_argument('-n',
                    '--numberOfImages',
                    type=int,
                    description='Number of images to look for')

args = parser.parse_args

# For each image:
#  Split a video every [args.frames] frames
#  && stick the image in between the two parts
# Then export it as args.video
def embedImages():
    """Embed images in video"""
    video = VideoFileClip(args.video)
    clips = []
    currentTime = 0

    # Populate list with subclips and pictures
    for image in args.images:
        firstPartDuration = args.seconds - 0.01
        secondPartStart = args.seconds + 0.01

        firstPart = video.subclip(currentTime, firstPartDuration)
        secondPart = video.subclip(secondPartStart, video.duration)
        insertFrame = ImageClip(image, duration=0.01)
        
        clips.append(firstPart)
        clips.append(insertFrame)
        clips.append(secondPart)

    # Build video from clips && save to args.output
    outfile = concatenate_videoclips(clips)
    outfile.write_videofile(args.output)

# For every n seconds in video
# until number of images is reached:
#  grab that frame && save it w/ the frame number as the path
def deembedImages():
    """Get images embedded in video"""
    video = VideoFileClip(args.video)
    currentImage = 0
    currentTime = args.seconds
    
    while currentImage != numberOfImages:
        imagePath = args.images[currentImage]
        frame = ImageClip
        
        video.save_frame(imagePath)
        
        currentTime += args.seconds
        currentImage += 1

# Don't run script if it is imported as a module
if __name__ == '__main__':
    if args.embed:
        embedImages()
    elif args.deembed:
        deembedImages()
    else:
        print('Specify embed/deembed')
