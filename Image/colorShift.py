"""Shift rgb values of pixels to encode data."""

'''
    stools
    Copyright (C) 2018 Samuel First

    This file is part of stools.

    stools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    stools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with stools.  If not, see <https://www.gnu.org/licenses/>.
'''

from binascii import hexlify
from PIL import Image
import argparse

# Define Arguments
#  Positional:
#   input file: name of file to read from
#  Flags:
#   -e: encode data
#   -d: decode data
#   -i: name of original image file
#   -m: string w/ message to encode
#   -o: name of file to write new image to
#   -r: write to red value of each pixel
#   -g: write to green value of each pixel
#   -b: write to blue value of each pixel
#     Note: default behavior is to write to
#           all three values
#   -n: print message instead of saving to file
parser = argparse.ArgumentParser(
    description='Hides Data by shifting rgb values')

parser.add_argument('input_file',
                    type=str,
                    help='Image file to encode/decode')

parser.add_argument('-e',
                    '--encode',
                    action='store_true',
                    help='Encode file with data')

parser.add_argument('-d',
                    '--decode',
                    action='store_true',
                    help='Decode data from file')

parser.add_argument('-i',
                    '--originalImage',
                    type=str,
                    help='Path to original image')

parser.add_argument('-m',
                    '--message', 
                    type=str,
                    help='Message to encode in file')

parser.add_argument('-o', 
                    '--out',
                    type=str,
                    help='Filename of output file')

parser.add_argument('-r', 
                    '--red',
                    action='store_true',
                    help='Write/read red value')

parser.add_argument('-g',
                     '--green',
                     action='store_true',
                     help='Write/read green value')

parser.add_argument('-b',
                    '--blue',
                    action='store_true',
                    help='Write/read blue value')

parser.add_argument('-n',
                    '--noSave',
                    action='store_true',
                    help='Print to standard I/O\
                    instead of saving to file')

args = parser.parse_args()

# Return int w/ number of colors used
def getNumberOfColors():
    """Get how many colors are used"""
    colors = 0

    if args.red:
        colors += 1
    if args.green:
        colors += 1
    if args.blue:
        colors += 1

    return colors

# Compare length of data && length of image
# If image < data then return false, otherwise return true
def sanityCheck():
    """Checks to make sure the data can fit in the file."""
    multiplier = getNumberOfColors()

    bitsToFile = ((args.input_file.size[0]
                   +args.input_file.size[1])
                  *multiplier)

    if args.message != None:
        if (len(args.message) * 7) > bitsToFile:
            return False
        else:
            return True
            
# Convert a string to int, then to bits
def convertToBits(string):
    """Convert a string to bits"""
    binString = ''
    
    for character in string:
        intChar = ord(character)
        binString += bin(intChar)[2:]  # The [2:] is to chop off the b'

    return binString

# Convert every byte (7 bits b/c ascii) to its associated character
# Then append each character to message && return message
def convertFromBits(string):
    """Convert bits to a string"""
    stringLength = len(string) - 1  # -1 so no off by one
    textString = ''

    for i in range(0, stringLength, 7):
        charValue = int(string[i:i+7], base=2)
        textString += chr(charValue)
        
    return textString

# Check if a bit is the last bit in the message
# If it is return true, otherwise, false
def isLastBit(data, bit):
    """Check if current bit is last bit of message"""
    if bit == len(data) - 1:
        return True
    else:
        return False

# Encode data with message
# Convert data to bits
# Then loop through file && shift the rgb values for each bit
#  (add/subtract bit to/from each value)
#   The reason we subtract some is that we can't write anything
#    larger than the max value of each pixel
# Then save the new image to args.out
def encode(image):
    """Encode image with data and write out to file"""
    data = convertToBits(args.message)
    image = Image.open(args.input_file)
    pixels = image.load()
    
    redMaxValue = image.getextrema()[0][1]
    greenMaxValue = image.getextrema()[1][1]
    blueMaxValue = image.getextrema()[2][1]
    
    yRange = image.size[1]
    xRange = image.size[0]
    doneEncoding = False
    currentBit = 0

    for y in range(yRange):
        if doneEncoding:
            break

        for x in range(xRange):
            red = pixels[x,y][0]
            green = pixels[x,y][1]
            blue = pixels[x,y][2]

            if args.red:
                if red == redMaxValue:
                    red -= int(data[currentBit])
                else:
                    red += int(data[currentBit])

                if isLastBit(data, currentBit):
                    doneEncoding = True
                    break
                else:
                    currentBit += 1

            if args.green:
                if green == greenMaxValue:
                    green -= int(data[currentBit])
                else:
                    green += int(data[currentBit])

                if isLastBit(data, currentBit):
                    doneEncoding = True
                    break
                else:
                    currentBit += 1

            if args.blue:
                if blue == blueMaxValue:
                    blue -= int(data[currentBit])
                else:
                    blue += int(data[currentBit])

                if isLastBit(data, currentBit):
                    doneEncoding = True
                    break
                else:
                    currentBit += 1

            image.putpixel((x,y), (red, green, blue))

    image.save(args.out)

# Decode data && print message
# Loop through specified bits until args.decodeSize
# Split bits into bytes && decode bytes
def decode(image):
    """Read data from image and decode"""
    originalImage = Image.open(args.originalImage)
    originalImagePixels = originalImage.load()

    encodedImage = Image.open(args.input_file)
    encodedImagePixels = encodedImage.load()

    yRange = encodedImage.size[1]
    xRange = encodedImage.size[0]
    doneDecoding = False
    data = ''

    # Compare the encoded file to the original
    # If color value is not the value in encoded:
    #  bit = 1
    # If it's the same:
    #  bit = 0
    # When we reach a pixel which is the same in
    #  both files, we know we've reached the end
    for y in range(yRange):
        if doneDecoding:
            break
        for x in range(xRange):
            encodedRed = encodedImagePixels[x,y][0]
            originalRed = originalImagePixels[x,y][0]

            encodedGreen = encodedImagePixels[x,y][1]
            originalGreen = originalImagePixels[x,y][1]

            encodedBlue = encodedImagePixels[x,y][2]
            originalBlue = originalImagePixels[x,y][2]

            if args.red:
                if encodedRed != originalRed:
                    data += '1'
                else:
                    data += '0'
                
                if encodedImagePixels[x,y] == originalImagePixels[x,y]:
                    doneDecoding = True
                    break
                
            if args.green:
                if encodedGreen != originalGreen:
                    data += '1'
                else:
                    data += '0'
                
                if encodedImagePixels[x,y] == originalImagePixels[x,y]:
                    doneDecoding = True
                    break
                    
            if args.blue:
                if encodedBlue != originalBlue:
                    data += '1'
                else:
                    data += '0'
                
                if encodedImagePixels[x,y] == originalImagePixels[x,y]:
                    doneDecoding = True
                    break

    message = convertFromBits(data)

    # Either Print decoded message or
    #  save it to file depending on what
    #  the user specifies
    if args.noSave:
        print(message)
    else:
        with open(args.out, 'w') as outFile:
            outFile.write()


# Only run if script is not imported
if __name__ == '__main__':
    image = Image.open(args.input_file)

    if args.encode:
        encode(image)
    elif args.decode:
        decode(image)
