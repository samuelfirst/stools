# stools

Set of steganography tools.

## Dependencies

* Pillow
  * (sudo) pip3 install Pillow

* Moviepy
  * (sudo) pip3 install moviepy

## Tools

### Image

* `colorShift.py`: hide data in rgb values of pixels

### Audio

### Video

### Miscellaneous
